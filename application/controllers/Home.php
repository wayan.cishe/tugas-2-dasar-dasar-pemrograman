<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
 
	public function __construct() 
	{
		parent::__construct();
		$this->load->model('M_Barang');
	}

	public function index()
	{
		$queryAllBarang = $this->M_Barang->getDataBarang();
		$DATA = array('queryAllBrng' => $queryAllBarang);
		$this->load->view('home', $DATA);
	}
	public function deret_angka()
	{
		$this->load->view('deret_angka');
	}
	public function hitungDeret()
	{
		$angka = $this->input->post('angka');
		$DATA = array('angka' => $angka);
		$this->load->view('hasil_deret', $DATA);
	}

	public function halaman_tambah()
	{
		$this->load->view('halaman_tambah');
	}
	
	public function halaman_edit($id)
	{
		$queryBarangDetail = $this->M_Barang->getDataBarangDetail($id);
		$DATA = array('queryBrngDetail' => $queryBarangDetail);
		$this->load->view('halaman_edit', $DATA);
	}

	public function fungsiTambah()
	{
		$nama = $this->input->post('nama');
		$quantity = $this->input->post('quantity');
		$harga = $this->input->post('harga');

		$ArrInsert = array(
			'harga' => $harga,
			'nama' => $nama,
			'quantity' => $quantity
		);

		$this->M_Barang->insertDataBarang($ArrInsert);
		redirect(base_url(''));

	}

	public function fungsiEdit()
	{
		$id = $this->input->post('id');
		$nama = $this->input->post('nama');
		$harga = $this->input->post('harga');
		$quantity = $this->input->post('quantity');

		$ArrUpdate = array(
			'nama' => $nama,
			'harga' => $harga,
			'quantity' => $quantity
		);

		$this->M_Barang->updateDataBarang($id, $ArrUpdate);
		redirect(base_url(''));

	}

	public function fungsiDelete($id)
	{
		$this->M_Barang->deleteDataBarang($id);
		redirect(base_url(''));
	}
}

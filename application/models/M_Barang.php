<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Barang extends CI_Model {

	function getDataBarang() {
		$query = $this->db->get('m_barang');
		return $query->result();
	}

	function insertDataBarang($data) {
		$this->db->insert('m_barang', $data);
	}

	function getDataBarangDetail($id) {
		$this->db->where('id', $id);
		$query = $this->db->get('m_barang');
		return $query->row();
	}

	function updateDataBarang($id, $data) {
		$this->db->where('id', $id);
		$this->db->update('m_barang', $data);
	}

	function deleteDataBarang($id) {
		$this->db->where('id', $id);
		$this->db->delete('m_barang');
	}

}

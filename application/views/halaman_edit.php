<!DOCTYPE html>
<html>
<head>
	<title>Halaman Edit Barang</title>
</head>
<body>
	<h3>Halaman Edit Barang</h3>
	<form action="<?php echo base_url('home/fungsiEdit') ?>" method="post">
	<table>
		<tr>
			<td>ID</td>
			<td>:</td>
			<td><input type="text" name="id" value="<?php echo $queryBrngDetail->id ?>" readonly></td>
		</tr>
		<tr>
			<td>Nama</td>
			<td>:</td>
			<td><input type="text" name="nama" value="<?php echo $queryBrngDetail->nama ?>" readonly></td>
		</tr>
		<tr>
			<td>Jumlah</td>
			<td>:</td>
			<td><input type="text" name="harga" value="<?php echo $queryBrngDetail->harga ?>"></td>
		</tr>
		<tr>
			<td>Quantity</td>
			<td>:</td>
			<td><input type="text" name="quantity" value="<?php echo $queryBrngDetail->quantity ?>"></td>
		</tr>
		<tr>
			<td colspan="3"><button type="submit">Update Barang</button></td>
		</tr>
	</table>
	</form>
</body>
</html>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE-edge">
	<title>Tugas 2 Pencatatan Barang</title>
	<link rel="stylesheet" href="">
</head>
<body>
	<h1> ( Wayan Cishe F. S. 2021062004 )</h1>
	<h1>DATA BARANG</h1>
	<button onclick="location.href='<?php echo base_url('home/halaman_tambah') ?>'" type="button">
	Tambah Barang</button>
	<br>
	<br>
	<table border="1">
		<tr>
			<td>No</td>
			<td>Nama</td>
			<td>Harga</td>
			<td>Quantity</td>
			<td>Operasi</td>
		</tr>
		<?php 
		
			$count = 0;
			foreach ($queryAllBrng as $row) {
				$count = $count + 1;
		 ?>
		<tr>
			<td><?php echo $count ?></td>
			<td><?php echo $row->nama ?></td>
			<td><?php echo $row->harga ?></td>
			<td><?php echo $row->quantity ?></td>
			<td><a href="<?php echo base_url('/home/halaman_edit') ?>/<?php echo $row->id ?>">Edit</a> | <a href="<?php echo base_url('/home/fungsiDelete') ?>/<?php echo $row->id ?>">Delete</a></td>
		</tr>
		<?php } ?>
	</table>

</body>
</html>